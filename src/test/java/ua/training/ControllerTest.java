package ua.training;

import junit.framework.Assert;
import org.junit.Test;

import java.util.List;
import java.util.Scanner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ControllerTest {

    @Test
    public void testCheckCorrectNumber() {
        Model model = new Model();
        GamePlayService service = new GamePlayService(model, new View());
        final int CORRECT_NUMBER = model.getSecretNumber();
        assertTrue(service.checkNumber(CORRECT_NUMBER));
    }

    @Test
    public void testCheckWrongNumber() {
        Model model = new Model();
        GamePlayService service = new GamePlayService(model, new View());
        final int WRONG_NUMBER = GlobalConstants.RAND_MAX + 1;
        assertFalse(service.checkNumber(WRONG_NUMBER));
    }

    @Test
    public void testProcessUserWithStubObject() {
        Model model = new Model();
        View view = new View();
        GamePlayService service = new GamePlayServiceUnderTest(model, view);
        Controller controller = new Controller(model, view, service);
        int expectedAttemptsCounter = 10;
        int actualAttemptsCounter = controller.processUser();
        Assert.assertEquals(expectedAttemptsCounter, actualAttemptsCounter);
    }

    private class GamePlayServiceUnderTest extends GamePlayService {
    private TestNumbers testNumbers;
    private List<Integer> numbersList;
    private int index;

        public GamePlayServiceUnderTest(Model model, View view) {
            super(model, view);
            testNumbers = new TestNumbers();
            numbersList = testNumbers.getTestNumbers(model.getSecretNumber());
        }

        @Override
        protected boolean isInputValid(Scanner scan) {
            return true;
        }

        protected int getIntInput(Scanner scan) {
            if (!numbersList.isEmpty()) {
                index++;
                return numbersList.get(index);
            }
            return -1;
        }
    }
}
