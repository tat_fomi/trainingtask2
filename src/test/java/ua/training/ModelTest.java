package ua.training;

import static org.junit.Assert.*;
import org.junit.Test;
import java.util.stream.IntStream;

public class ModelTest {
    private Model model;

    @Test
    public void testRandFromZeroToHundred() {
        model = new Model();
        final int MIN_VALUE = 0;
        final int MAX_VALUE = 100;
        final int RANDOM_NUMBERS_COUNTER = 200;
        IntStream stream = IntStream.iterate(1, i -> (model.rand(MIN_VALUE, MAX_VALUE)));
        long counter = stream
                .limit(200)
                .filter(i -> (i >= MIN_VALUE && i <= MAX_VALUE))
                .count();
        assertEquals(RANDOM_NUMBERS_COUNTER, counter);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRandWithBiggerMinValue() {
        model = new Model();
        final int MIN_VALUE  = 100;
        final int MAX_VALUE = 0;
        model.rand(MIN_VALUE, MAX_VALUE);
    }

    @Test
    public void testRandMinEqualsMax() {
        model = new Model();
        final int MIN_VALUE = 100;
        final int MAX_VALUE = 100;
        final int EXPECTED_RANDOM_NUMBER = 100;
        int actual =  model.rand(MIN_VALUE, MAX_VALUE);
        assertEquals(EXPECTED_RANDOM_NUMBER,actual);
    }

}
