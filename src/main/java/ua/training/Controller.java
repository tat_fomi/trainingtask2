package ua.training;

import java.util.Scanner;

public class Controller {
    private Model model;
    private View view;
    private GamePlayService gamePlayService;

    public Controller(Model model, View view) {
        this.model  = model;
        this.view = view;
        gamePlayService = new GamePlayService(model, view);
    }

    public Controller(Model model, View view, GamePlayService gamePlayService) {
        this.model  = model;
        this.view = view;
        this.gamePlayService = gamePlayService;
    }

    public int processUser() {
        view.startGame();
        if (gamePlayService.gamePlay()) {
            int attempts = model.getAttemptCounter();
            view.congratulate(attempts);
            return attempts;
        }
        return -1;
    }


}
