package ua.training;

import java.util.Scanner;

public class GamePlayService {
    private Model model;
    private View view;

    public GamePlayService(Model model, View view) {
        this.model  = model;
        this.view = view;
    }

    public boolean gamePlay() {
        Scanner scan = new Scanner(System.in);
        if (checkNumber(checkIntInput(scan))) {
            return true;
        } else {
            return gamePlay();
        }
    }

    public boolean checkNumber(int attempt) {
        model.setAttemptCounter(model.getAttemptCounter()+1);
        if (attempt == model.getSecretNumber()) {
            return true;
        }
        model.getEnteredNumbers().add(attempt);
        model.moveRangeBorder(attempt);
        return false;
    }

    private int checkIntInput(Scanner scan) {
        showGameMessages();
        while (!isInputValid(scan)) {
            view.showInputError();
            view.setAction("not a number");
            model.setAttemptCounter(model.getAttemptCounter() + 1);
            scan.next();
        }
        int inputVal = getIntInput(scan);
        if (inputVal <= model.getMaxValue() && inputVal >= model.getMinValue()) {
            view.setAction(String.valueOf(inputVal));
            return inputVal;
        }
        view.showNotInRangeMessage(inputVal);
        view.setAction("a number out of range");
        model.setAttemptCounter(model.getAttemptCounter() + 1);
        return checkIntInput(scan);
    }

    protected boolean isInputValid(Scanner scan) {
        return scan.hasNextInt();
    }

    protected int getIntInput(Scanner scan) {
        return scan.nextInt();
    }

    private void showGameMessages() {
        view.showStatistics(model.getEnteredNumbers());
        view.showQuestionMessage(model.getMinValue(), model.getMaxValue());
        if (view.getAction()!=null) {
            view.showPreviousAction();
        }
    }
}
