package ua.training;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/** Class Model contains method randomize(), that generates secret number, checking methods and statistics.
 */
public class Model {
    private List <Integer> enteredNumbers;
    private int secretNumber;
    private int minValue;
    private int maxValue;
    private int attemptCounter;

    public Model() {
        enteredNumbers = new ArrayList<>();
        secretNumber = rand();
    }

    public void moveRangeBorder(int attempt) {
        if (secretNumber > attempt) {
            setMinValue(attempt);
        } else {
            setMaxValue(attempt);
        }
    }

    public int rand() {
        minValue = GlobalConstants.RAND_MIN;
        maxValue = GlobalConstants.RAND_MAX;
        return generateRandom(minValue, maxValue);
    }

    public int rand(int minValue, int maxValue) {
        if (minValue > maxValue ) {
            throw (new IllegalArgumentException("MIN parameter should be smaller than MAX"));
        }
        if (minValue == maxValue) {
            return maxValue;
        }
        this.minValue = minValue;
        this.maxValue = maxValue;
        return generateRandom(minValue, maxValue);
    }

    private int generateRandom(int minValue, int maxValue) {
        Random random = new Random();
        secretNumber = random.nextInt(maxValue - minValue + 1) + minValue;
        return secretNumber;
    }

    public List<Integer> getEnteredNumbers() {
        return enteredNumbers;
    }

    public int getSecretNumber() {
        return secretNumber;
    }

    public int getMinValue() {
        return minValue;
    }

    public void setMinValue(int minValue) {
        this.minValue = minValue;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }

    public int getAttemptCounter() {
        return attemptCounter;
    }

    public void setAttemptCounter(int attemptCounter) {
        this.attemptCounter = attemptCounter;
    }
}
