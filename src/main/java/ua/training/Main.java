package ua.training;

/**
 * Created by Tetianka on 16.04.2017.
 */
public class Main {
    public static void main(String[] args) {
        Model model = new Model();;
        Controller controller = new Controller(model, new View());
        controller.processUser();

        }
   }
