package ua.training;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Tetianka on 18.04.2017.
 */
public class TestNumbers {
    List<Integer> list;

    public List<Integer> getTestNumbers(int secretNumber) {
        list = new ArrayList<>();
        Random random = new Random();
        int minValue = GlobalConstants.RAND_MIN;
        int maxValue = GlobalConstants.RAND_MAX;
        int counter = 0;
        while (counter < 10) {
            int generated = random.nextInt(maxValue - minValue + 1) + minValue;
            if (generated != secretNumber) {
                list.add(generated);
                counter++;
            }
        }
        list.add(secretNumber);
        return list;

    }




}
