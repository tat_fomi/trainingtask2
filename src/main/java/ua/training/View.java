package ua.training;

import java.util.ArrayList;
import java.util.List;

/**
 * Class View contains lines, constants and methods to combine them, to guide user through the game.
 */
public class View {
    private final String WELCOME_MESSAGE = "Welcome to Java-game";
    private final String RULE = "Try to guess the secret number in the range";
    private final String CONGRATULATION = "Congratulations! You've divined the secret number";
    private String action;

    public void startGame() {
        printMessage(WELCOME_MESSAGE);
    }

    public void showQuestionMessage(int randMin, int randMax) {
        printMessage(RULE + " from " + randMin + " to " + randMax );
    }

    public void showInputError() {
        printMessage("You haven't entered a number. Please try again");
    }

    public void showStatistics(List userNumbers) {
        if (!userNumbers.isEmpty()) {
            printMessage(userNumbers.toString() + " - you have already tested");
        }
    }

    public void congratulate(int userCounter) {
        printMessage(CONGRATULATION + " with " + userCounter + ((userCounter>1) ? " attempts" : " attempt"));
    }

    public void showNotInRangeMessage(int inputVal) {
        printMessage(inputVal + " is out of range!");
    }

    private void printMessage(String message) {
        System.out.println(message);
    }

    public void showPreviousAction() {
        printMessage("Previously you have entered " + action);
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

}
